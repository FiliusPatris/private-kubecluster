# Private Kubecluster

This is my kubernetes cluster for private use.

## Usage
Don't try to use it yourself as a complete unit.
It is not inteded to be run by other people than myself,
thus all configuration would need to be adjusted for yourself.

You may lookup some stuff for your project or copy some yaml files of some software stacks,
but as the license states: No warranty at all.

I actually use this cuslter btw.

## Contents
This repo contains serveral prepared software stacks as solutions for many different needs:

- [ ] Blogging: [writefreely](https://writefreely.org/)
- [ ] Music streaming: [koel](https://koel.dev/)
- [ ] Knowledge base: [Outline](https://getoutline.com/)
- [ ] Static site: Uhm, well just some static sites?
- [ ] Analytics: [Plausible](https://plausible.io/)